/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// OpenGL ES 2.0 code

#include <jni.h>
#include <android/log.h>

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

// glm
// http://www.opengl-tutorial.org/jp/beginners-tutorials/tutorial-3-matrices/
// 0.9.5 only?
// #define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
// 回転行列用
#include <glm/gtx/transform.hpp>

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
// value_ptr
#include <glm/gtc/type_ptr.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// pcl

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>

#define  LOG_TAG    "libgl2jni"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

/**
 * ファイルサイズの取得
 */
fpos_t fgetsize(FILE* stream) {

    // 現在のファイル位置を取得
    fpos_t current = 0;
    fgetpos(stream, &current);

    // ファイルの終端へ移動
    fseek(stream, 0, SEEK_END);

    // シーク位置を取得
    fpos_t size = 0;
    fgetpos(stream, &size);

    // ファイルの元の位置へ戻る
    fseek(stream, current, SEEK_SET);

    return size;
}

static void printGLString(const char *name, GLenum s) {
    const char *v = (const char *) glGetString(s);
    LOGI("GL %s = %s\n", name, v);
}

static void checkGlError(const char* op) {
    for (GLint error = glGetError(); error; error
            = glGetError()) {
        LOGI("after %s() glError (0x%x)\n", op, error);
    }
}

// "  gl_Position = vPosition;\n"
// "  gl_Position = uMVPMatrix * vPosition;\n"
auto gVertexShader =
    "attribute vec4 vPosition;\n"
    "uniform mat4 uMVPMatrix;\n"
    "void main() {\n"
    "  gl_Position = uMVPMatrix * vPosition;\n"
    "  gl_PointSize = 3.0;\n"
    "}\n";

auto gFragmentShader =
    "precision mediump float;\n"
    "void main() {\n"
    "  gl_FragColor = vec4(0.0, 1.0, 0.0, 1.0);\n"
    "}\n";

GLuint loadShader(GLenum shaderType, const char* pSource) {
    GLuint shader = glCreateShader(shaderType);
    if (shader) {
        glShaderSource(shader, 1, &pSource, NULL);
        glCompileShader(shader);
        GLint compiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            GLint infoLen = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
            if (infoLen) {
                char* buf = (char*) malloc(infoLen);
                if (buf) {
                    glGetShaderInfoLog(shader, infoLen, NULL, buf);
                    LOGE("Could not compile shader %d:\n%s\n",
                            shaderType, buf);
                    free(buf);
                }
                glDeleteShader(shader);
                shader = 0;
            }
        }
    }
    return shader;
}

GLuint createProgram(const char* pVertexSource, const char* pFragmentSource) {
    GLuint vertexShader = loadShader(GL_VERTEX_SHADER, pVertexSource);
    if (!vertexShader) {
        return 0;
    }

    GLuint pixelShader = loadShader(GL_FRAGMENT_SHADER, pFragmentSource);
    if (!pixelShader) {
        return 0;
    }

    GLuint program = glCreateProgram();
    if (program) {
        glAttachShader(program, vertexShader);
        checkGlError("glAttachShader");
        glAttachShader(program, pixelShader);
        checkGlError("glAttachShader");
        glLinkProgram(program);
        GLint linkStatus = GL_FALSE;
        glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
        if (linkStatus != GL_TRUE) {
            GLint bufLength = 0;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &bufLength);
            if (bufLength) {
                char* buf = (char*) malloc(bufLength);
                if (buf) {
                    glGetProgramInfoLog(program, bufLength, NULL, buf);
                    LOGE("Could not link program:\n%s\n", buf);
                    free(buf);
                }
            }
            glDeleteProgram(program);
            program = 0;
        }
    }
    return program;
}

GLuint gProgram;
GLuint gvPositionHandle;
GLuint guMVPMatrixHandle;

static pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
static pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);

glm::mat4 viewAndProjectionMatrix;
glm::mat4 projectionMatrix;
glm::mat4 viewMatrix;
glm::mat4 modelMatrix;
bool initTransform = false;

bool setupGraphics(int w, int h) {
    printGLString("Version", GL_VERSION);
    printGLString("Vendor", GL_VENDOR);
    printGLString("Renderer", GL_RENDERER);
    printGLString("Extensions", GL_EXTENSIONS);

    LOGI("setupGraphics(%d, %d)", w, h);
    gProgram = createProgram(gVertexShader, gFragmentShader);
    if (!gProgram) {
        LOGE("Could not create program.");
        return false;
    }

    gvPositionHandle = glGetAttribLocation(gProgram, "vPosition");
    checkGlError("glGetAttribLocation");
    LOGI("glGetAttribLocation(\"vPosition\") = %d\n", gvPositionHandle);

    guMVPMatrixHandle = glGetUniformLocation(gProgram, "uMVPMatrix");
    checkGlError("glGetUniformLocation");
    LOGI("glGetUniformLocation(\"uMVPMatrix\") = %d\n", guMVPMatrixHandle);

    // glUniformMatrix4fv(gModelviewHandle, 1, false, m, 0);
    glViewport(0, 0, w, h);
    checkGlError("glViewport");

    float ratio=(float)w/h;
    float left  =   -ratio;
    float right =   ratio;
    float bottom=   -1.0f;
    float top   =   1.0f;
    float near  =   0.1f;
    float far   =   1000.0f;

    // 射影行列
    // 画面の座標系を作成
    projectionMatrix = glm::frustum(left, right, bottom, top, near, far);

    // ビュー行列?
    // ワールド空間でのカメラの位置
    float eyeX=0.0f;
    float eyeY=0.0f;
    // Z : + 側縮小, -側拡大
    // float eyeZ=1.0f;
    // float eyeZ=3.0f; // 左右 1/3?
    float eyeZ=1.5f;
    glm::vec3 eye_vec(eyeX, eyeY, eyeZ);

    // ワールド空間での見たい位置
    // オブジェクトの中心位置？
    float centerX=0.0f;
    float centerY=0.0f;
    float centerZ=0.0f;
    glm::vec3 center_vec(centerX, centerY, centerZ);

    // ???
    // (0, 1,0) を使用。
    // (0,-1,0) で上下さかさまになる。
    float upX=0.0f;
    float upY=1.0f;
    float upZ=0.0f;
    glm::vec3 up_vec(upX, upY, upZ);
    // カメラの向きを作成
    viewMatrix = glm::lookAt(eye_vec, center_vec, up_vec);

    // 上記２つを合算。
    // matrix.multiplyMM(mViewAndProjectionMatrix, 0, projectionMatrix, 0, viewMatrix, 0);
    // viewAndProjectionMatrix = projectionMatrix * viewMatrix;

    return true;
}

void loadPCDFile()
{
    // Fill in the cloud data
#if 1
    cloud->width = 1000;
    cloud->height = 1;
    cloud->points.resize(cloud->width * cloud->height);

    for (size_t i = 0; i < cloud->points.size(); ++i) {
        cloud->points[i].x = 1024 * rand() / (RAND_MAX + 1.0f);
        cloud->points[i].y = 1024 * rand() / (RAND_MAX + 1.0f);
        cloud->points[i].z = 1024 * rand() / (RAND_MAX + 1.0f);
    }
#else
    // ファイルからの読出し
    // あらかじめ、ADM によりファイルを転送しておく。
    // 6.0 の場合、permission の確認を行うこと
    // 読み込みOK :
    std::string pcl_file = "storage/emulated/0/lamppost.pcd";
    // NG(データサイズが原因？)
    // std::string pcl_file = "storage/emulated/0/bun0.pcd";
    // std::string pcl_file = "storage/emulated/0/CSite1_orig-utm.pcd";

    // /data/data/(package) folder にファイルを設定
    // std::string pcl_file = "lamppost.pcd";
    // ng : boost library?
    // 大きいサイズもng : 点群として 大体 20k程 まで
    if (pcl::io::loadPCDFile<pcl::PointXYZ> (pcl_file, *cloud) == -1) //* load the file
    {
        // PCL_ERROR ("Couldn't read file test_pcd.pcd.\n");
        return;
    }
#endif
    // Create the filtering object
    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud (cloud);
    // pass.setFilterFieldName ("z");
    pass.setFilterFieldName ("y");
    pass.setFilterLimits (0.0, 0.01);
    pass.setFilterLimitsNegative (true);
    pass.filter (*cloud_filtered);
}

const GLfloat gTriangleVertices[] = { 0.0f, 0.5f, -0.5f, -0.5f, 0.5f, -0.5f };
const GLfloat gPointVertices[] = { 0.0f, 0.5f, -0.5f };


void renderFrame()
{
    if (cloud == nullptr) return;
    if (cloud_filtered == nullptr) return;

    int count = cloud_filtered->size();
    // int count = cloud->size();
    // GLfloat (*point)[3] = (GLfloat (*)[3])glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    GLfloat point[(count + 1) * 3]; //(GLfloat (*)[3])glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    for (int i = 0; i < count; i++)
    {
        // -1.0 ～ 1.0
        point[i * 3 + 0] = cloud_filtered->points[i].x;
        point[i * 3 + 1] = cloud_filtered->points[i].y;
        point[i * 3 + 2] = cloud_filtered->points[i].z;
        /*
        point[i * 3 + 0] = cloud->points[i].x;
        point[i * 3 + 1] = cloud->points[i].y;
        point[i * 3 + 2] = cloud->points[i].z;
        */
    }

    static float grey = 0.0f;
    static float angle = 0.0f;
    // grey += 0.01f;
    angle += 0.02f;
    // if (grey > 1.0f)
    // {
    //     grey = 0.0f;
    // }
    if (angle > 3.14f)
    {
        angle = -3.14f;
    }

    // すでに単位行列のため不要?
    // modelMatrix = glm::identity();
    //Push to the distance - note this will have no effect on a point size
    // glm::vec3 transtate_vec(0.0f, 0.0f, -5.0f);
    // 平行移動
    // x軸
    // y軸
    // z軸 + : 手前に移動, - : 奥に移動
    glm::vec3 transtate_vec;
    if (initTransform == false)
    {
        // transtate_vec = glm::vec3(10.104160f, -0.074005f, 2.144748f);
        // transtate_vec = glm::vec3(2.144748f, 10.104160f, -0.074005f);
        transtate_vec = glm::vec3(-10.104160f, 0.074005f, 0.0f);
        initTransform = true;
    }
    else
    {
        transtate_vec = glm::vec3(0.0f, 0.0f, 0.0f);
    }
    // lamppost : mass_center , -10.104160 0.074005 -2.144748.
    // glm::vec3 transtate_vec(10.104160f, -0.074005f, 2.144748f);
    // glm::vec3 transtate_vec(2.144748f, 10.104160f, -0.074005f);
    modelMatrix = glm::translate(modelMatrix, transtate_vec);

    // 回転
    // 軸を指定 = 1.0f?
    // glm::vec3 myRotationAxis(1.0f, 0.0f, 0.0f);
    // y軸固定
    glm::vec3 myRotationAxis(0.0f, 1.0f, 0.0f);
    glm::mat4x4 myRotationMatrix = glm::rotate(angle, myRotationAxis);

    // glm::rotate(viewMatrix, angle,
    // Matrix.multiplyMV(mMVPMatrix, 0, mViewMatrix, 0, mModelMatrix, 0);
    // Matrix.multiplyMV(mMVPMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);
    // glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);

    // viewMatrix からの平行移動
    // viewAndProjectionMatrix = viewMatrix * modelMatrix;
    // 回転追加
    viewAndProjectionMatrix = viewMatrix * modelMatrix * myRotationMatrix;
    // Camera? の表示領域の設定
    viewAndProjectionMatrix = projectionMatrix * viewAndProjectionMatrix;
    // glUniformMatrix4fv(guMVPMatrixHandle, 1, false, (GLfloat*)viewAndProjectionMatrix.tmat4x4());
    glUniformMatrix4fv(guMVPMatrixHandle, 1, false, glm::value_ptr(viewAndProjectionMatrix));

    glClearColor(grey, grey, grey, 1.0f);
    checkGlError("glClearColor");
    glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    checkGlError("glClear");

    glUseProgram(gProgram);
    checkGlError("glUseProgram");

    // Sample
    /*
    glVertexAttribPointer(gvPositionHandle, 2, GL_FLOAT, GL_FALSE, 0, gTriangleVertices);
    checkGlError("glVertexAttribPointer");
    glEnableVertexAttribArray(gvPositionHandle);
    checkGlError("glEnableVertexAttribArray");
    glDrawArrays(GL_TRIANGLES, 0, 3);
    // glDrawArrays(GL_POINTS, 0, 3);
    checkGlError("glDrawArrays");
    */

    // 頂点属性の格納場所と書式を指定
    glVertexAttribPointer(gvPositionHandle, 3, GL_FLOAT, GL_FALSE, 0, point);
    // glVertexAttribPointer(gvPositionHandle, 3, GL_FLOAT, GL_FALSE, 0, gPointVertices);
    checkGlError("glVertexAttribPointer");

    // 視線座標変更
    // float* worldMatrix = new float[16];
    // Matrix.setIdentityM(worldMatrix, 0);
    // Matrix.rotateM(worldMatrix, 0, (float)mFrameCount / 2f, 0, 0, 1);
    // glUniformMatrix4fv(uniLoc1, 1, false, mViewAndProjectionMatrix, 0);
    // glUniformMatrix4fv(uniLoc2, 1, false, worldMatrix, 0);
    // 画面の座標系を作成
    // カメラの向きを作成
    // 上記２つを合算。
    // glUniformMatrix4fv(guMVPMatrixHandle, 1, false, mViewAndProjectionMatrix.get());

    // gvPositionHandle を有効にする
    glEnableVertexAttribArray(gvPositionHandle);
    checkGlError("glEnableVertexAttribArray");

    // 点群の表示
    glDrawArrays(GL_POINTS, 0, count);
    // glDrawArrays(GL_POINTS, 0, 1);
    checkGlError("glDrawArrays");

    // 描画する毎に視点を変更する場合?
    // glDisableVertexAttribArray(gvPositionHandle);
}

extern "C" {
    JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNILib_init(JNIEnv * env, jobject obj,  jint width, jint height);
    JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNILib_step(JNIEnv * env, jobject obj);
    JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNILib_load(JNIEnv * env, jobject obj);
};

JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNILib_init(JNIEnv * env, jobject obj,  jint width, jint height)
{
    setupGraphics(width, height);
}

JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNILib_step(JNIEnv * env, jobject obj)
{
    renderFrame();
}

JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNILib_load(JNIEnv * env, jobject obj)
{
    loadPCDFile();
}