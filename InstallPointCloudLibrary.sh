
$ZIP_FILE = 'pcl-superbuild-x86.zip'
if [ -e $ZIP_FILE ]; then
    # 存在する場合
    echo "File $CFG_FILE exists."
else
    # 存在しない場合
    wget https://ci.appveyor.com/api/buildjobs/lrsfpvvy7d7576y0/artifacts/$ZIP_FILE
fi

mkdir -p app/jniLibs/x86
cd app/jniLibs/x86
unzip -o ../../../pcl-superbuild-x86.zip
cd ../../../



ZIP_FILE = ='pcl-superbuild-arm64-v8a.zip'
if [ -e ZIP_FILE ]; then
    # 存在する場合
    echo "File $CFG_FILE exists."
else
    # 存在しない場合
    wget https://ci.appveyor.com/api/buildjobs/64oer708j0dr3c1l/artifacts/$ZIP_FILE
fi

mkdir -p app/jniLibs/arm64-v8a
cd app/jniLibs/arm64-v8a
unzip -o ../../../pcl-superbuild-arm64-v8a.zip
cd ../../../



ZIP_FILE = ='pcl-superbuild-armeabi-v7a.zip'
if [ -e $ZIP_FILE ]; then
    # 存在する場合
    echo "File $ZIP_FILE exists."
else
    # 存在しない場合
    wget https://ci.appveyor.com/api/buildjobs/ha0lfikov8q4my0o/artifacts/$ZIP_FILE
fi

mkdir -p app/jniLibs/armeabi-v7a

cd app/jniLibs/armeabi-v7a
unzip -o ../../../pcl-superbuild-armeabi-v7a.zip
cd ../../../
